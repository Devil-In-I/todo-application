﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace todo_domain_entities
{
    public class TodoList
    {
        /// <summary>
        /// Id of a list.
        /// </summary>
        [Key]
        public int ListId { get; set; }
        /// <summary>
        /// Name of a list.
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// Flag to now wether to show list or hide.
        /// </summary>
        [Required]
        public bool IsHided { get; private set; }

        /// <summary>
        /// Collection of items of current Todo List.
        /// </summary>
        public List<TodoItem> TodoItems { get; set; }

        /// <summary>
        /// Parametrized constructor.
        /// </summary>
        /// <param name="name">Name of a todo list. Can not be null or empty. Length should be between 1 and 25 characters.</param>
        /// <param name="isHided">Flag to know if a TodoList needs to be hided or not.</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public TodoList(string name, bool isHided = false)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name), "Name cannot be null or empty.");
            }
            else if (name.Length > 25)
            {
                throw new ArgumentException("Length of the name must be no more than 25 chars.", nameof(name));
            }

            IsHided = isHided;
            Name = name;
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public TodoList()
        {
        }

        /// <summary>
        /// Sets IsHided property of a list to true.
        /// </summary>
        public void HideList()
        {
            this.IsHided = true;
        }

        /// <summary>
        /// Sets IsHided property of a list to false.
        /// </summary>
        public void ShowList()
        {
            this.IsHided = false;
        }
    }
}
