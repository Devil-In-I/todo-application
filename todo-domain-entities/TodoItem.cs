﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace todo_domain_entities
{
    public enum ItemStatuses
    {
        Completed = 0,
        InProgress = 1,
        NotStarted = 2
    }

    public class TodoItem
    {
        /// <summary>
        /// ID of an entry.
        /// </summary>
        [Key]
        public int EntryID { get; set; }

        /// <summary>
        /// Title of an entry.
        /// </summary>
        [Required]
        public string Title { get; set; }

        /// <summary>
        /// Description of an entry.
        /// </summary>
        [Required]
        public string Description { get; set; }

        /// <summary>
        /// Date of creation of an entry.
        /// </summary>
        [Required]
        public DateTime CreationDate { get; set; } = DateTime.Now;

        /// <summary>
        /// Deadline date for an entry.
        /// </summary>
        [Required]
        public DateTime DueDate { get; set; }

        /// <summary>
        /// Status of an entry.
        /// </summary>
        [Required]
        public virtual int ItemStatusId { 
            get
            {
                return (int)this.ItemStatus;
            }
            set
            {
                ItemStatus = (ItemStatuses)value;
            }
        }

        [EnumDataType(typeof(ItemStatuses))]
        public ItemStatuses ItemStatus { get; set; } = ItemStatuses.NotStarted;

        /// <summary>
        /// Foreign key.
        /// </summary>
        public int ListID { get; set; }

        /// <summary>
        /// Navigation property.
        /// </summary>
        public TodoList TodoList { get; set; }

        /// <summary>
        /// Parametrized constructor.
        /// </summary>
        /// <param name="title">Title of a Todo item. Must be non-empty not null string. Shouldn't be more than 25 characters.</param>
        /// <param name="description">Description for Todo item. Cannot be null or empty. Length should be less than 125 characters.</param>
        /// <param name="dueDate">Due date of an item. Should be more than CreationDate. But can't be DateTime.MaxValue.</param>
        /// <param name="list">List to which related this item. Can't be null</param>
        /// <param name="status">Status of an item. (NotStarted, InProgress, Completed)</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public TodoItem(
            string title,
            string description,
            DateTime dueDate,
            TodoList list,
            ItemStatuses status = ItemStatuses.NotStarted)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentNullException(nameof(title), "Title cannot be null or empty.");
            }
            else if (title.Length > 25)
            {
                throw new ArgumentException("Length of the title must be no more than 25 chars.", nameof(title));
            }

            if (string.IsNullOrEmpty(description))
            {
                throw new ArgumentNullException(nameof(description), "Description cannot be null or empty.");
            }
            else if (description.Length > 125)
            {
                throw new ArgumentException("Length of the description must be no more than 125 chars.", nameof(description));
            }

            if (dueDate <= DateTime.Now)
            {
                throw new ArgumentException("Due date should be more than DateTime.Now.", nameof(dueDate));
            }
            else if (dueDate == DateTime.MaxValue)
            {
                throw new ArgumentException("Due date cannot be must value.", nameof(dueDate));
            }
            

            if (list == null)
            {
                throw new ArgumentNullException(nameof(list), "List cannot be null.");
            }

            Title = title;
            Description = description;
            DueDate = dueDate;
            ItemStatus = status;
            TodoList = list;
        }
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        public TodoItem()
        {
        }
    }
}
