﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using todo_aspnetmvc_ui.Models;
using todo_aspnetmvc_ui.Models.Repo;
using todo_aspnetmvc_ui.Models.ViewModels;
using todo_domain_entities;

namespace todo_aspnetmvc_ui.Controllers
{
    public class TodoListController : Controller
    {
        private readonly ITodoRepository _todoRepository;
        private readonly IItemRepository _itemRepository;
        private readonly IReminderRepository _reminderRepository;

        public TodoListController(ITodoRepository repository, IItemRepository itemRepository, IReminderRepository reminderRepository)
        {
            this._todoRepository = repository;
            this._itemRepository = itemRepository;
            this._reminderRepository = reminderRepository;
        }

        [HttpGet]
        public async Task<IActionResult> Index(ListIndexViewModel model)
        {
            var lists = from l in await _todoRepository.GetListsAsync()
                        select l;

            var reminders = from r in await _reminderRepository.GetRemindersAsync()
                            select r;

            model.Lists = lists;

            if (model.ShowHidedLists != true)
            {
                model.Lists = lists.Where(x => x.IsHided == false);
            }

            model.Reminders = reminders;
            model.CheckboxDescription = "Show hided lists?";

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Details(TodoListDetailsViewModel model, [FromRoute] int id)
        {
            var list = await _todoRepository.GetTodoListByIdAsync(id);
            var items = list.TodoItems;

            model.List = list;

            if (model.HideCompletedItems == true)
            {
                items = items.Where(x => x.ItemStatus != ItemStatuses.Completed).ToList();
            }

            if (model.ShowWithDueToday == true)
            {
                items = items.Where(x => x.DueDate.Date == DateTime.Today).ToList();
            }

            if (model.HideExpired == true)
            {
                items = items.Where(x => x.DueDate.Date >= DateTime.Today).ToList();
            }

            model.List.TodoItems = items;
            model.HideCompletedDescription = "Hide completed todo items?";
            model.ShowWithDueTodayDescription = "Show todo items with due date today?";
            model.HideExpiredDescription = "Hide expired items?";

            return View(model);
        }


        [HttpGet]
        public ViewResult Create()
        {
            return View();
        }

        //public ActionResult Create(TodoList list)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            _todoRepository.InsertTodoList(list);
        //            _todoRepository.Save();
        //            return RedirectToAction("Index");
        //        }
        //    }
        //    catch (DataException /*dex*/)
        //    {

        //        ModelState.AddModelError(string.Empty, "Unable to save changes.Try again, and if the problem persists contact your system administrator.");
        //    }
        //    return View(list);
        //}
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(string name)
        {
            ViewData["NameOfListParam"] = String.IsNullOrEmpty(name) ? "" : name;
            try
            {
                TodoList list = new TodoList(name);
                await _todoRepository.InsertTodoListAsync(list);
                await _todoRepository.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception)
            {
                return RedirectToAction(nameof(Index));
            }
        }

        public async Task<IActionResult> Edit(int id)
        {
            TodoList list = await _todoRepository.GetTodoListByIdAsync(id);
            return View(list);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(TodoList list)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _todoRepository.UpdateTodoList(list);
                    await _todoRepository.SaveAsync();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {

                ModelState.AddModelError(string.Empty, "Unable to save changes.Try again, and if the problem persists contact your system administrator.");
            }
            return View(list);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            TodoList list = await _todoRepository.GetTodoListByIdAsync((int)id);
            if (list == null)
            {
                return NotFound();
            }
            return View(list);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            TodoList list = await _todoRepository.GetTodoListByIdAsync(id);
            if (list == null)
            {
                RedirectToAction(nameof(Index));
            }

            try
            {
                await _todoRepository.DeleteTodoListAsync(id);
                await _todoRepository.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DataException)
            {
                return RedirectToAction(nameof(Delete), new { id });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Hide(int id)
        {
            TodoList list = await _todoRepository.GetTodoListByIdAsync(id);
            list.HideList();
            _todoRepository.UpdateTodoList(list);
            await _todoRepository.SaveAsync();
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Show(int id)
        {
            TodoList list = await _todoRepository.GetTodoListByIdAsync(id);
            list.ShowList();
            _todoRepository.UpdateTodoList(list);
            await _todoRepository.SaveAsync();
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Copy(int id)
        {
            var list = await _todoRepository.GetTodoListByIdAsync(id);
            if (list == null)
            {
                return NotFound();
            }

            var copiedList = new TodoList(list.Name, list.IsHided);

            await _todoRepository.InsertTodoListAsync(copiedList);

            await _todoRepository.SaveAsync();

            //var items = list.TodoItems;

            //List<TodoItem> itemsCopied = new List<TodoItem>();

            //list = (await _todoRepository.GetListsAsync()).Last();

            //foreach (var item in items)
            //{
            //    itemsCopied.Add(new TodoItem(item.Title, item.Description, DateTime.Today.AddDays(5), list, item.ItemStatus));
            //}

            //await _itemRepository.InsertItemsRangeAsync(itemsCopied);

            return RedirectToAction(nameof(Index));
        }

        protected override void Dispose(bool disposing)
        {
            _todoRepository.Dispose();
            _itemRepository.Dispose();
            _reminderRepository.Dispose();
            base.Dispose(disposing);
        }
    }
}
