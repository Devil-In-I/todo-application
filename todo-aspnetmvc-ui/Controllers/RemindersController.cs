﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using todo_aspnetmvc_ui.Models;
using todo_aspnetmvc_ui.Models.Repo;

namespace todo_aspnetmvc_ui.Controllers
{
    public class RemindersController : Controller
    {
        private readonly IReminderRepository _reminderRepo;
        private readonly IItemRepository _itemRepo;

        public RemindersController(IReminderRepository reminderRepo, IItemRepository itemRepo)
        {
            _reminderRepo = reminderRepo;
            _itemRepo = itemRepo;
        }

        [HttpPost]
        [Route("Reminders/Create/{itemId:int}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int itemId)
        {
            var item = await _itemRepo.GetItemByIdAsync(itemId);
            if (item == null)
            {
                return NotFound();
            }
            var reminder = new Reminder(item.Title, itemId);

            await _reminderRepo.InsertReminderAsync(reminder);
            await _reminderRepo.SaveAsync();

            return RedirectToRoute("default", new { controller = "TodoList", action = "Details", id = item.ListID });
        }

        [HttpPost]
        [Route("Reminders/Delete/{itemId:int}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int itemId)
        {
            var item = await _itemRepo.GetItemByIdAsync(itemId);
            if (item == null)
            {
                return NotFound();
            }

            await _reminderRepo.RemoveReminderAsync(itemId);
            await _reminderRepo.SaveAsync();

            return RedirectToRoute("default", new { controller = "TodoList", action = "Details", id = item.ListID });
        }

        protected override void Dispose(bool disposing)
        {
            _itemRepo.Dispose();
            _reminderRepo.Dispose();
            base.Dispose(disposing);
        }
    }
}
