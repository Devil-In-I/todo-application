﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using todo_aspnetmvc_ui.Models.Repo;
using todo_aspnetmvc_ui.Models.ViewModels;
using todo_domain_entities;

namespace todo_aspnetmvc_ui.Controllers
{
    public class TodoItemController : Controller
    {
        private readonly IItemRepository _itemRepository;
        private readonly ITodoRepository _todoRepository;

        public TodoItemController(IItemRepository itemRepository, ITodoRepository todoRepository)
        {
            this._itemRepository = itemRepository;
            this._todoRepository = todoRepository;
        }

        [HttpGet]
        [Route("TodoItem/Create/{listId:int}")]
        public async Task<IActionResult> Create(int listId)
        {
            var list = await _todoRepository.GetTodoListByIdAsync(listId);
            if (list == null)
            {
                return NotFound();
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("TodoItem/Create/{listId:int}")]
        public async Task<IActionResult> Create(TodoItem item, [FromRoute]int listId)
        {
            var list = await _todoRepository.GetTodoListByIdAsync(listId);

            if (list == null)
            {
                return NotFound();
            }

            try
            {
                item.CreationDate = DateTime.Now;

                if (ModelState.IsValid)
                {
                    await _itemRepository.InsertItemAsync(item);
                    await _itemRepository.SaveAsync();

                    return RedirectToRoute("default", new { controller = "TodoList", action = "Details", id = listId });
                }
            }
            catch (DataException /*dex*/)
            {

                ModelState.AddModelError(string.Empty, "Unable to save changes.Try again, and if the problem persists contact your system administrator.");
            }
            return View(item);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            TodoItem item = await _itemRepository.GetItemByIdAsync(id);
            List<TodoList> lists = await _todoRepository.GetListsAsync();
            var viewModel = new EditItemViewModel(item, lists);
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(TodoItem item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _itemRepository.UpdateItem(item);
                    await _itemRepository.SaveAsync();
                    return RedirectToRoute("default", new { controller = "TodoList", action = "Details", id = item.ListID });
                }
            }
            catch (DataException)
            {

                ModelState.AddModelError(string.Empty, "Unable to save changes.Try again, and if the problem persists contact your system administrator.");
            }
            return View(item);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            TodoItem item = await _itemRepository.GetItemByIdAsync((int)id);
            if (item == null)
            {
                return NotFound();
            }
            return View(item);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            TodoItem item = await _itemRepository.GetItemByIdAsync(id);
            if (item == null)
            {
                RedirectToAction(nameof(Index));
            }

            try
            {
                await _itemRepository.DeleteItemAsync(id);
                await _itemRepository.SaveAsync();
                return RedirectToRoute("default", new { controller = "TodoList", action = "Details", id = item.ListID });
            }
            catch (DataException)
            {
                return RedirectToAction(nameof(Delete), new { id });
            }
        }

        protected override void Dispose(bool disposing)
        {
            _todoRepository.Dispose();
            _itemRepository.Dispose();
            base.Dispose(disposing);
        }
    }
}
