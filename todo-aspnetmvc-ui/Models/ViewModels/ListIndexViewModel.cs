﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using todo_domain_entities;

namespace todo_aspnetmvc_ui.Models.ViewModels
{
    public class ListIndexViewModel
    {
        public bool ShowHidedLists { get; set; }

        public string CheckboxDescription { get; set; }

        public IEnumerable<TodoList> Lists { get; set; }

        public IEnumerable<Reminder> Reminders { get; set; }
    }
}
