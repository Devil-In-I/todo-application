﻿using System.Collections.Generic;
using todo_domain_entities;

namespace todo_aspnetmvc_ui.Models.ViewModels
{
    public class TodoListDetailsViewModel
    {
        public string HideCompletedDescription { get; set; }

        public string ShowWithDueTodayDescription { get; set; }

        public string HideExpiredDescription { get; set; }

        public bool HideCompletedItems { get; set; }

        public bool ShowWithDueToday { get; set; }

        public bool HideExpired { get; set; }

        public TodoList List { get; set; }
    }
}
