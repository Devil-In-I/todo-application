﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using todo_domain_entities;

namespace todo_aspnetmvc_ui.Models.ViewModels
{
    public class EditItemViewModel
    {
        public TodoItem Item { get; set; }
        public List<TodoList> Lists { get; set; }

        /// <summary>
        /// View model for Edit action of controller TodoItem to pass collection of <see cref="TodoList"/> type entities into Edit view.
        /// </summary>
        /// <param name="item">Todo item to edit.</param>
        /// <param name="lists">All existing todo lists from db.</param>
        /// <exception cref="ArgumentNullException">Throws an exception if parametrs are null.</exception>
        public EditItemViewModel(TodoItem item, List<TodoList> lists)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item), "Item cannot be null.");
            }
            else if (lists == null)
            {
                throw new ArgumentNullException(nameof(lists), "Lists cannot be null.");
            }

            Item = item;
            Lists = lists;
        }
    }
}
