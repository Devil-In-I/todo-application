﻿using Microsoft.EntityFrameworkCore;
using todo_domain_entities;

namespace todo_aspnetmvc_ui.Models
{
    public class TodoDbContext : DbContext
    {
        public TodoDbContext(DbContextOptions options)
            : base(options){}

        public DbSet<TodoList> TodoLists { get; set; }
        public DbSet<TodoItem> TodoItems { get; set; }
        public DbSet<Reminder> Reminders { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TodoItem>(e =>
            {
                e.HasOne(i => i.TodoList).WithMany(l => l.TodoItems)
                .HasForeignKey(i => i.ListID);
            });
            base.OnModelCreating(modelBuilder);
        }

    }
}
