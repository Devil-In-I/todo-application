﻿using System;
using System.ComponentModel.DataAnnotations;

namespace todo_aspnetmvc_ui.Models
{
    public class Reminder
    {
        /// <summary>
        /// Id of reminder. Primary key.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Title of reminder.
        /// </summary>
        [Required]
        public string Title { get; set; }

        /// <summary>
        /// Id of item for which this reminder is created.
        /// </summary>
        [Required]
        public int ItemId { get; set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Reminder()
        {

        }

        /// <summary>
        /// Parametrized constructor for Reminder.
        /// </summary>
        /// <param name="title">Title of reminder. Title can not be null.</param>
        /// <param name="itemId">Id of item for which this reminder is created. Item id can not be less than or equal to 0.</param>
        /// <exception cref="ArgumentNullException">Thrown when title is null or empty.</exception>
        /// <exception cref="ArgumentException">Thrown when itemId <= 0</exception>
        public Reminder(string title, int itemId)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentNullException(nameof(title), "Title can not be null.");
            }
            else if (itemId <= 0)
            {
                throw new ArgumentException("Item id can not be less than or equal to 0.");
            }

            Title = title;
            ItemId = itemId;
        }
    }
}
