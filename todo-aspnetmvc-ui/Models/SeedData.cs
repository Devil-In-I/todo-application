﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using todo_domain_entities;

namespace todo_aspnetmvc_ui.Models
{
    public static class SeedData
    {
        public static void EnsurePopulated(IApplicationBuilder app)
        {
            TodoDbContext ctx = app.ApplicationServices.CreateScope().ServiceProvider.GetRequiredService<TodoDbContext>();

            if (ctx.Database.GetPendingMigrations().Any())
            {
                ctx.Database.Migrate();
            }

            if (!ctx.TodoLists.Any() && !ctx.TodoItems.Any())
            {
                var lists = new List<TodoList>();
                lists.AddRange(new TodoList[]
                { 
                    new TodoList("Test list 1"),
                    new TodoList("Test list 2"),
                    new TodoList("Test list 3"),
                    new TodoList("Test list 4"),
                    new TodoList("Test list 5") 
                });
                ctx.TodoLists.AddRange(lists);

                var items = new List<TodoItem>();
                items.AddRange(new TodoItem[] {
                    new TodoItem("test item 2", "test description 2", DateTime.Now.AddDays(6), lists[1]),
                    new TodoItem("test item 1", "test description 1", DateTime.Now.AddDays(5), lists[0]),
                    new TodoItem("test item 3", "test description 3", DateTime.Now.AddDays(7), lists[2]),
                    new TodoItem("test item 9", "test description 9", DateTime.Now.AddDays(8), lists[3]),
                    new TodoItem("test item 4", "test description 4", DateTime.Now.AddDays(8), lists[4]),
                    new TodoItem("test item 5", "test description 5", DateTime.Now.AddDays(9), lists[0]),
                    new TodoItem("test item 6", "test description 6", DateTime.Now.AddDays(4), lists[1]),
                    new TodoItem("test item 7", "test description 7", DateTime.Now.AddDays(5), lists[2]),
                    new TodoItem("test item 8", "test description 8", DateTime.Now.AddDays(6), lists[3]),
                    new TodoItem( "test item 10","test description 10", DateTime.Now.AddDays(10), lists[4])
                });
                ctx.TodoItems.AddRange(items);

                ctx.SaveChanges();
            }
        }
    }
}
