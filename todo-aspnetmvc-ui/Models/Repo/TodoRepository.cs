﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using todo_domain_entities;

namespace todo_aspnetmvc_ui.Models.Repo
{
    public class TodoRepository : ITodoRepository, IDisposable
    {
        private readonly TodoDbContext db;

        public async Task<List<TodoList>> GetListsAsync()
        {
            var lists = await db.TodoLists.Include(x => x.TodoItems)
                .AsNoTracking().ToListAsync();
            return lists;
        }

        public TodoRepository(TodoDbContext context)
        {
            this.db = context;
        }

        public async Task<TodoList> GetTodoListByIdAsync(int listID)
        {
            var list = await db.TodoLists.Include(x => x.TodoItems)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.ListId == listID);
            return list;
        }

        public async Task InsertTodoListAsync(TodoList list)
        {
            await this.db.TodoLists.AddAsync(list);
        }

        public async Task DeleteTodoListAsync(int listID)
        {
            var list =  await db.TodoLists.FindAsync(listID);
            db.TodoLists.Remove(list);
        }

        public void UpdateTodoList(TodoList list)
        {
            db.Entry(list).State = EntityState.Modified;
        }

        public async Task SaveAsync()
        {
            await db.SaveChangesAsync();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }

              this.disposed = true;
            }
        }

        void IDisposable.Dispose()
        {
            // Не изменяйте этот код. Разместите код очистки в методе "Dispose(bool disposing)".
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
