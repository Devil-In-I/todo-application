﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace todo_aspnetmvc_ui.Models.Repo
{
    public class ReminderRepository : IReminderRepository, IDisposable
    {
        private readonly TodoDbContext db;

        public ReminderRepository(TodoDbContext db)
        {
            this.db = db;
        }

        /// <summary>
        /// Asynchronously returns all reminders from database.
        /// </summary>
        /// <returns><see cref="Task"/> with List of reminders.</returns>
        public async Task<List<Reminder>> GetRemindersAsync()
        {
            return await db.Reminders.ToListAsync();
        }

        /// <summary>
        /// Asynchronously inserts new reminder to a database.
        /// </summary>
        /// <param name="reminder">A new reminder to be added to database.</param>
        /// <returns><see cref="Task"/></returns>
        public async Task InsertReminderAsync(Reminder reminder)
        {
            await db.Reminders.AddAsync(reminder);
        }

        /// <summary>
        /// Asynchronously removes reminder from a database by itemId property for a specific TodoItem. 
        /// </summary>
        /// <param name="itemId">Id of an item, for which the reminder was created.</param>
        /// <returns><see cref="Task"/></returns>
        public async Task RemoveReminderAsync(int itemId)
        {
            var reminder = await db.Reminders.FirstOrDefaultAsync(x => x.ItemId == itemId);
            if (reminder != null)
            {
                db.Reminders.Remove(reminder);
            }
        }

        /// <summary>
        /// Saves changes after actions performed on db context.
        /// </summary>
        /// <returns><see cref="Task"/></returns>
        public async Task SaveAsync()
        {
            await db.SaveChangesAsync();
        }

        private bool disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }

                this.disposed = true;
            }
        }

        public void Dispose()
        {
            // Не изменяйте этот код. Разместите код очистки в методе "Dispose(bool disposing)".
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
