﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using todo_domain_entities;

namespace todo_aspnetmvc_ui.Models.Repo
{
    public interface IItemRepository : IDisposable
    {
        Task<List<TodoItem>> GetItemsAsync();

        Task<TodoItem> GetItemByIdAsync(int itemID);
        Task InsertItemAsync(TodoItem item);
        Task InsertItemsRangeAsync(IEnumerable<TodoItem> items);
        Task DeleteItemAsync(int itemID);
        void UpdateItem(TodoItem item);
        Task SaveAsync();
    }
}
