﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using todo_domain_entities;

namespace todo_aspnetmvc_ui.Models.Repo
{
    public interface ITodoRepository : IDisposable
    {
        Task<List<TodoList>> GetListsAsync();

        Task<TodoList> GetTodoListByIdAsync(int listID);
        Task InsertTodoListAsync(TodoList list);
        Task DeleteTodoListAsync(int listID);
        void UpdateTodoList(TodoList list);
        Task SaveAsync();
    }
}
