﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace todo_aspnetmvc_ui.Models.Repo
{
    public interface IReminderRepository : IDisposable
    {
        Task<List<Reminder>> GetRemindersAsync();

        Task InsertReminderAsync(Reminder reminder);

        Task RemoveReminderAsync(int itemId);

        Task SaveAsync();
    }
}
