﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using todo_domain_entities;

namespace todo_aspnetmvc_ui.Models.Repo
{
    public class ItemRepository : IItemRepository, IDisposable
    {
        private readonly TodoDbContext db;

        public ItemRepository(TodoDbContext db)
        {
            this.db = db;
        }

        public async Task<List<TodoItem>> GetItemsAsync()
        {
            return await db.TodoItems.ToListAsync();
        }

        public async Task<TodoItem> GetItemByIdAsync(int itemID)
        {
            return await db.TodoItems.FindAsync(itemID);
        }

        public async Task InsertItemAsync(TodoItem item)
        {
            await db.TodoItems.AddAsync(item);
        }

        public async Task InsertItemsRangeAsync(IEnumerable<TodoItem> items)
        {
            await db.TodoItems.AddRangeAsync(items);
        }

        public async Task DeleteItemAsync(int itemID)
        {
            var item = await db.TodoItems.FindAsync(itemID);
            db.TodoItems.Remove(item);
        }

        public void UpdateItem(TodoItem item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public async Task SaveAsync()
        {
            await db.SaveChangesAsync();
        }

        private bool disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }

                this.disposed = true;
            }
        }

        public void Dispose()
        {
            // Не изменяйте этот код. Разместите код очистки в методе "Dispose(bool disposing)".
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
