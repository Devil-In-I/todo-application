﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit;
using NUnit.Framework;
using todo_domain_entities;

namespace todo_app_tests
{
    [TestFixture]
    public class TodoList_Tests
    {
        [Test]
        public void TodoList_Constructor_ParametrsAreValid()
        {
            // Arrange.
            string name = "test list";
            TodoList list = new TodoList(name);

            // Assert.
            Assert.AreEqual(list.Name, name);
        }

        [TestCase("12345678901234567890123456789012345678901234567890")]
        public void TodoList_Constructor_ParametrsAreNotValid_ThrowsArgumentException(string name)
        {
            // Arrange.
            TodoList list;

            // Assert.
            Assert.Throws<ArgumentException>(() =>
            {
                list = new TodoList(name);
            });
        }

        [TestCase("")]
        [TestCase(null)]
        public void TodoList_Constructor_ParametrsAreNotValid_ThrowsArgumentNullException(string name)
        {
            // Arrange.
            TodoList list;

            // Assert.
            Assert.Throws<ArgumentNullException>(() =>
            {
                list = new TodoList(name);
            });
        }
    }
}
