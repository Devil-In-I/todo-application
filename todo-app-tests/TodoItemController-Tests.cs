﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using todo_aspnetmvc_ui.Controllers;
using todo_aspnetmvc_ui.Models.Repo;
using todo_domain_entities;
using Xunit;

namespace todo_app_tests
{
    public class TodoItemController_Tests
    {
        /// <summary>
        /// Test method for Create action of TodoList controller.
        /// it test if the method returns NotFound result when passing wrong id and list not found, => list == null.
        /// </summary>
        [Fact]
        public async Task Create_ReturnsNotFound()
        {
            // Arrange
            TodoList list = new TodoList("test#1");
            var item = new TodoItem("Test", "Test", DateTime.Now.AddDays(5), list);
            var TodoRepoMock = new Mock<ITodoRepository>();
            var ItemRepoMock = new Mock<IItemRepository>();
            TodoRepoMock.Setup(x => x.GetTodoListByIdAsync(123)).ReturnsAsync((TodoList)null);
            var controller = new TodoItemController(ItemRepoMock.Object, TodoRepoMock.Object);
            // Act
            var result = await controller.Create(item, list.ListId);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        /// <summary>
        /// Test method for HttpGET Edit action of TodoList controller.
        /// It test if the method returns ViewResult.
        /// Parameters are valid - ViewResult should be returned.
        /// </summary>
        [Fact]
        public async Task Edit_ReturnsViewResult()
        {
            // Arrange
            TodoList list = new TodoList("test#1");
            var item = new TodoItem("Test", "Test", DateTime.Now.AddDays(5), list);
            var TodoRepoMock = new Mock<ITodoRepository>();
            var ItemRepoMock = new Mock<IItemRepository>();

            TodoRepoMock.Setup(x => x.GetListsAsync()).ReturnsAsync(GetTestLists());
            ItemRepoMock.Setup(x => x.GetItemByIdAsync(1)).ReturnsAsync(item);

            var controller = new TodoItemController(ItemRepoMock.Object, TodoRepoMock.Object);

            // Act
            var result = await controller.Edit(1);

            // Assert
            Assert.IsType<ViewResult>(result);
        }

        private List<TodoList> GetTestLists()
        {
            var lists = new List<TodoList>
            {
                new TodoList("List #1"),
                new TodoList("List #2"),
                new TodoList("List #3"),
                new TodoList("List #4"),
                new TodoList("List #5")
            };
            return lists;
        }

        private List<TodoItem> GetTestItems(List<TodoList> lists)
        {
            List<TodoItem> items = new List<TodoItem>();
            int i = 1;
            foreach (var list in lists)
            {
                items.Add(new TodoItem($"Item#{i}", $"item#{i} desc", DateTime.Today.AddDays(5), list));
                i++;
            }
            return items;
        }

    }
}
