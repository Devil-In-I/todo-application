﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using todo_domain_entities;

namespace todo_app_tests
{
    [TestFixture]
    public class TodoItem_Tests
    {
        [Test]
        public void TodoItem_Constructor_ParametrsAreValid()
        {
            // Arrange.
            string title = "test";
            string description = "abcdefg";
            DateTime dueDate = DateTime.Now.AddDays(5);
            TodoList list = new TodoList("test");
            TodoItem item = new TodoItem(title, description, dueDate, list);

            // Assert.
            Assert.Multiple(() =>
            {
                Assert.AreEqual(title, item.Title);
                Assert.AreEqual(description, item.Description);
                Assert.AreEqual(dueDate, item.DueDate);
            });
        }

        [TestCase("12345678901234567890123456", "test", null)]
        [TestCase("test", "1234567890qwertyuiopasdfghjkl;[]'zxcvbnm.ZXCVBNM<>LKJHGFDSAQWERTYUIOP1234567890qwertyuiopasdfghjkl;[]'zxcvbnm.ZXCVBNM<>LKJHGFDSAQWERTYUIOP", null)]

        public void TodoItem_Constructor_ParametrsAreNotValid_ThrowsArgumentException(string title, string description, DateTime due)
        {
            // Assert.
            Assert.Throws<ArgumentException>(() =>
            {
                TodoList list = new TodoList("test");
                TodoItem item = new TodoItem(title, description, due, list);
            });
        }

        [TestCase("", "", null)]
        [TestCase("test", "", null)]
        public void TodoItem_Constructor_ParametrsAreNotValid_ThrowsArgumentNullException(string title, string description, DateTime due)
        {
            // Assert.
            Assert.Throws<ArgumentNullException>(() =>
            {
                TodoList list = new TodoList("test");
                TodoItem item = new TodoItem(title, description, due, list);
            });
        }
    }
}
