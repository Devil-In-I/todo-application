﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using todo_aspnetmvc_ui.Controllers;
using todo_aspnetmvc_ui.Models;
using todo_aspnetmvc_ui.Models.Repo;
using todo_aspnetmvc_ui.Models.ViewModels;
using todo_domain_entities;
using Xunit;

namespace todo_app_tests
{
    public class TodoListController_Tests
    {
        /// <summary>
        /// Test method for Index action of TodoList controller.
        /// it test if the method returns viewResult with viewModel containing list of TodoLists.
        /// </summary>
        [Fact]
        public async Task IndexReturnsViewResultWithAllTodoLists()
        {
            // Arrange
            var TodoRepoMock = new Mock<ITodoRepository>();
            var ItemRepoMock = new Mock<IItemRepository>();
            var reminderRepoMock = new Mock<IReminderRepository>();
            var lists = GetTestLists();
            var items = GetTestItems(lists);
            TodoRepoMock.Setup(x => x.GetListsAsync()).ReturnsAsync(lists);
            reminderRepoMock.Setup(x => x.GetRemindersAsync()).ReturnsAsync(new List<Reminder> { });
            var controller = new TodoListController(TodoRepoMock.Object, ItemRepoMock.Object, reminderRepoMock.Object);

            // Act
            var result = await controller.Index(new ListIndexViewModel
            {
                ShowHidedLists = false
            });

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<ListIndexViewModel>(viewResult.ViewData.Model);
            Assert.Equal(lists.Count, model.Lists.Count());
        }

        /// <summary>
        /// Test method for HttpGET Create action of todoList controller.
        /// It test if the method returns viewResult.
        /// Should return viewResult.
        /// </summary>
        [Fact]
        public void CreateReturnsViewReult()
        {
            // Arrange
            var TodoRepoMock = new Mock<ITodoRepository>();
            var ItemRepoMock = new Mock<IItemRepository>();
            var reminderRepoMock = new Mock<IReminderRepository>();
            var controller = new TodoListController(TodoRepoMock.Object, ItemRepoMock.Object, reminderRepoMock.Object);

            // Act
            var result =  controller.Create();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
        }

        private List<TodoList> GetTestLists()
        {
            var lists = new List<TodoList>
            {
                new TodoList("List #1"),
                new TodoList("List #2"),
                new TodoList("List #3"),
                new TodoList("List #4"),
                new TodoList("List #5")
            };
            return lists;
        }

        private List<TodoItem> GetTestItems(List<TodoList> lists)
        {
            List<TodoItem> items = new List<TodoItem>();
            int i = 1;
            foreach (var list in lists)
            {
                items.Add(new TodoItem($"Item#{i}", $"item#{i} desc", DateTime.Today.AddDays(5), list));
                i++;
            }
            return items;
        }
    }
}
